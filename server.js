var net = require('net');
var express = require('express');
var dataBase = require('./api/models/databaseModel');
var ip_adress = "192.168.0.11";
app = express();
var server = net.createServer(function (socket) {
    socket.setEncoding("ascii");
    socket.on('data', (e) => {
        if (e.length > 10) {
            e = e.replace("3:::","");
            e = e.replace(" ","");
            e = e.trim();
            if(e[e.length-1].charCodeAt(0)==127)
            {
                e = e.slice(0,e.length-1);
            }
            if(e[0].charCodeAt(0)==127)
            {
                e = e.slice(1,e.length);
            }
            try{
                var data = JSON.parse(e);
                if(data.token === "8RsriD5mQ465d7aG1OHNSQrAtyJ36t11")
                {
                    dataBase.putMeasurment(data.sensors);
                }
                else
                {
                    console.log("Error token is incorrect");
                }
            }
            catch(err)
            {
                console.log("Parsin error")
            }
        }
    })
    socket.on('end', (e) => {
       console.log("END");
    })
});
dataBase.connect();
var routes = require('./api/routes/dataRoutes');
routes(app);
app.listen(3001, ip_adress)
server.listen(3000, ip_adress);