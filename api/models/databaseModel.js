var mysql = require('mysql');

// var con = mysql.createConnection({
//   host: "mysql.home.amu.edu.pl",
//   user: "pf2_sandbox",
//   password: "HydroxySQL=1343",
//   database: "pf2_sandbox"
// });
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "mydb"
});
exports.connect = function () {

  con.connect(function (err) {
    if (err) {
      console.log("Connection error");
    }
    else {
      console.log("Connected!");
    }
  });
}

exports.putMeasurment = function (data) {
  var sql = "INSERT INTO measurments (pm01, pm10, pm25, temperature, hum) VALUES (" + data.pm01 + "," + data.pm10 + "," + data.pm25 + "," + data.temp + "," + data.hum + ")";
  con.query(sql, function (err, result) {
    if (err) throw err;
    return 0;
  });
}
exports.getMeasurments = function () {
  return new Promise(function (resolve, reject) {
    
    con.query("SELECT * FROM measurments", function (err, result, fields) {
      if (err) throw err;
      resolve(result);
    });
  })

}
exports.getMeasurmentsWithDate = function(datem) {
  var date = new Date(datem.year, datem.month, datem.day,0,0);
  console.log(date)
  return new Promise(function (resolve, reject) {
    con.query("SELECT * FROM measurments WHERE date >="+"'"+date.toLocaleDateString()+"'", function (err, result, fields) {
      if (err) throw err;
      resolve(result);
    });
  })

}
exports.get = function () {
  return con;
}