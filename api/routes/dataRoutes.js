module.exports = function (app) {
  var todoList = require('../controllers/dataController');
  var token = "EB2C781DA25F65A9EBB524AAC593F";

  var middleware = function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    var user_token = req.query.token;
    if (token !== user_token) {
      res.status(401).send("ERROR MSG");
    }
    else {
      next();
    }
  }
  app.route('/api/get-data')
    .get([middleware, todoList.get_data])
  app.route('/api/put_data')
    .get(todoList.putData);
  app.route('/api/remove-all')
    .post([middleware, todoList.remove_all])
  app.route('/api/restart')
    .post([middleware, todoList.restart_arduino])
  app.get('*', function (req, res) {
    res.status(404).send("WHAAAT");
  });
  app.post('*', function (req, res) {
    res.status(404).send("WHAAAT");
  });
};