var dataBase = require('../models/databaseModel');

// exports.get_data = function (req, res) {

//   dataBase.get().query("SELECT * FROM measurments", function (err, result, fields) {
//     if (err) throw err;
//     res.json(result);
//   });
// };

exports.get_data = function (req, res) {
  if (req.query.data) {
    console.log("Data is set");
    dataBase.getMeasurmentsWithDate(req.query)
      .then(function (data) {
        console.log(data.length)
        res.status(200).json({ "values": data });
      })
  }
  else {
    dataBase.getMeasurments()
      .then(function (data) {
        console.log(data.length)
        res.status(200).json({ "values": data });
      })
  }
}
exports.remove_all = function (req, res) {

}
exports.restart_arduino = function (req, res) {

}
exports.putData = function (req, res) {
  data = {
    "pm01": 15,
    "pm10": 20,
    "pm25": 30,
    "temperature": 20,
    "hum": 12
  }
  response = dataBase.putMeasurment(data);
  res.json({ "data": response });
}
